[Home](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/home.md?ref_type=heads)   | [About our project](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/dna.md?ref_type=heads) | [Team](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/Team/Team.md?ref_type=heads) | [Terminology](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/Team/terminology/Trem.md?ref_type=heads)
# New terms and concepts we learned.

### - Mechanics of DNA: Sequence-dependent DNA mechanics regulates DNA-deforming processes genome-wide.

### - TinkerCAD : It is a free-of-charge, online 3D modelling program that runs in a web browser. We used it to design both the circuit an the 3D model.

### - Fusion 360: Its a commercial computer-aided design, computer-aided manufacturing, computer-aided engineering and printed circuit design software.

### - Arduino Uno: It is an open source microcontroller board on the Microchip ATmega328P microcontroller and developed by Arduino.cc

### - Circuit designing:The process of circuit design can cover systems ranging from complex electronic systems down to the individual transistors within an integrated circuit.

# Softwares Used

### - Tinkercad
### - Fusion 360
### - GitLab


# Outcomes as Individuals
Premika Rai: I hope to have good teamwork and analytical skills by the end of the project.

Pranita Ghalley: I want to come up with innovative ideas to solve issues.

Rashmika Chhetri: I want to learn how to use Gitlab and also improve my teamwork skills.

Tandin Tashi Wangmo: I want to enhance my collaboration and problem solving skills.

Jigme Chogyel: I want the project of ours to be beneficial to students learning biology and enhance their knowlegde.

[Home](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/home.md?ref_type=heads)   | [About our project](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/dna.md?ref_type=heads) | [Team](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/Team/Team.md?ref_type=heads) | [Terminology](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/Team/terminology/Trem.md?ref_type=heads)



