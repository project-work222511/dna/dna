[Home](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/home.md?ref_type=heads)   | [About our project](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/dna.md?ref_type=heads) | [Team](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/Team/Team.md?ref_type=heads) | [Terminology](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/Team/terminology/Trem.md?ref_type=heads)
# MEET THE TEAM 

![Alt text](jc.jpg)
1. Name- Jigme Chogyel

Age- 17

Date of birth- 25-7-2006

Hobby- Dancing, drawing

What do you want to learn?- Iconography

Contribution to team: 3D Design and planning.


![Alt text](pg.jpg)
2. Parnita Ghalley

Age- 16

DAte of birth- 29-06-2007

Hobby- Exploring new topics through reading, poeple, and other sources

What do you want to know?- Any topics that interest me.

Contribution to team: Documentation.

![Alt text](pr.jpg)
3. Name- Premika Rai

Age- 17

Date of birth- 22-08-2006

Hobby- Playing sports, drawing

What do you want to learn? - 3D designing

Contribution to team: Drawing and circuit Design.

![Alt text](rc.jpg)
4. Rashmika Chhetri

Age- 16

Date of birth- 14-09-2007

Hobby- Painting

What do you want to learn?- Details of Gitlab and Fusion 360

Contribution to team: Drawing the model reference and 3D design.

![Alt text](ttw.jpg)

5. Name- Tandin Tashi Wangmo

Age- 16

Date of birth- 26-08-2007

Hobby- Reading, exploring

What do you want to learn? - Space, astronomy, physics.

Contribution to team: Researcher and Reference maker.

# Strategies for Peer - learning.

1. Teamwork: without teamwork, we would not have been able to develop a project on which everyone agreed and decided to work on. It also helped us to bond withe each other and help each other out in fields where one of us was good at.

2. Research: Through research, we were able to look for an effective idea and also develop a project which would benefit the targeted audience. 

3. Collaboration: through this, we were able to learn from each other and also solve problems related to softwares and designing.

[Home](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/home.md?ref_type=heads)   | [About our project](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/dna.md?ref_type=heads) | [Team](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/Team/Team.md?ref_type=heads) | [Terminology](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/Team/terminology/Trem.md?ref_type=heads)




