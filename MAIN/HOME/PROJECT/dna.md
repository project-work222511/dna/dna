[Home](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/home.md?ref_type=heads)   | [About our project](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/dna.md?ref_type=heads) | [Team](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/Team/Team.md?ref_type=heads) | [Terminology](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/Team/terminology/Trem.md?ref_type=heads)
# PROJECT IDEA

![Alt text](<Mega DNA.jpg>)


## 1. Firstly we made our ideas into a rough sketch.

![](work.jpg)

 We want to make the components of DNA by either lazer cutting or 3-D printing.WE want to make the componets fit each other by attaching them together.We will also 3-d print the helix.We want the 4 bases to glow by adding LED of different color to each.

## 2.Then we made our DNA model with specification of every segment of the DNA(Adenine, Guanine, cytosine and thymine). 


![Alt text](tinker.png)

![Alt text](side%20.png)

![Alt text](dna2.png)

-  Link to simulation in TinkerCAD,(https://www.tinkercad.com/things/dViCTVm0tge-fantabulous-duup-kup/edit?sharecode=ljN-68rblYu1eEHQ6MoExQhlDbS_1tte-joyfQjUtbU)


## 3. We designed reference working of the model.

![Alt text](sketch.png)

 The Motor that will spin the disk that hold the 3-D model will be connected to the Arduino board which will have the coding to spin the motor. The arduino board will be powered with a cable that can be plugged into a socket.

## 2. Then we designed our DC motor circuit with slide switch to control it, in the TinkerCAD. 

![Alt text](circuit2.png)

This is a circuit design of our project. We have a 9v battery that supplies power to the DC motor that will be connected to the DNA model. The speed of the DC motor will be controlled with the help of the potentiometer and the power on and off will be controlled with the switch.
- TinkerCAD simulation (https://www.tinkercad.com/things/58ILNk12Obp-project/editel)

### 

# Research 

![Alt text](motion.gif)

## _Did You Know?_

_DNA twists in response to bonding between two strands. This occurs as a result of DNA's spatial requirement. The protein histones that allow it to coil and supercoil. The AT bond has two hydrogen bonds, whereas the GC bond has three hydrogen bonds._

# Working concept
 When we switch on the slide switch, the DC motor will receive power from the battery and we will also be able to control the speed of the DC motor using potentiometer, and the DC motor will rotate our DNA model.
# Component Required:
1. DC Motor : A DC motor is an electrical device that converts direct current (DC) electrical energy into mechanical energy. It typically consists of a rotor (the rotating part) and a stator (the stationary part), with brushes and a commutator to provide electrical connections.

2. LED: Light-emitting diodes (LEDs) are semiconductor devices that emit light when an electric current passes through them. They come in various colors, including red, green, blue, and yellow. LEDs are commonly used in electronics for indicators, displays, and illumination.

3. Resistor (2, 150 ohm and 10 kiloohm): A resistor is a passive electronic component that limits the flow of electric current in a circuit. The 150-ohm resistor is typically used to limit the current flowing through LEDs to prevent them from burning out, while the 10-kilohm resistor can be used in various applications like pull-up or pull-down resistors in digital circuits or voltage dividers in analog circuits.

4. Potentiometer: Variable resistor with three terminals, used to adjust resistance and control voltage levels in circuits.

5. Switch: Simple electrical component that opens or closes a circuit, allowing or interrupting the flow of current, used for turning devices on/off or selecting between different modes/functions.




[Home](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/home.md?ref_type=heads)   | [About our project](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/dna.md?ref_type=heads) | [Team](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/Team/Team.md?ref_type=heads) | [Terminology](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/Team/terminology/Trem.md?ref_type=heads)

















