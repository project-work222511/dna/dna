[Home](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/home.md?ref_type=heads)   | [About our project](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/dna.md?ref_type=heads) | [Team](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/Team/Team.md?ref_type=heads) | [Terminology](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/Team/terminology/Trem.md?ref_type=heads)
# WELCOME ABOARD 
# THE GENE TECH TEAM
          Showing the DNA mechanics

![](./dn.jpeg)


# About Us 
### We are a group of students studying at The Royal Academy who hopes to solve the real life doubts of biology students, struggling with the motion of DNA. 

### Our group consists of:
- Jigme Chogyel
- Pranita Ghalley
- Premika Rai
- Rashmika Chhetri
- Tandin Tashi Wangmo

![](./grop.jpg)

# About Our Project
### Our project is to develop a fabricated 3-D model of a DNA which will help the students learning Biology to practically understand how the DNA moves in the reality. Through this, we hope to enhance the student's knowledge on Dna mobility and also the working of components such as Arduino and other motors and all.

![](./pro.jpg)


# Our Motivation
### The main motive that has driven us all forward to take this project is that we wanted to help those students learning biology to understand the actual mechanism of the human DNA. As all of our groupmates have once gone through this phase of having difficulties in understanding the real mechanism of DNA, we felt the need to have an actual model that depicts how the DNA really works. 

# In the near future
### We are looking forwards towards using this model in every biology learning experiences and letting the students understand not only the theory of the DNA but also the mechanics of the DNA. We can even use this model as the beginning of the era where we use our innovative ideas and the FabLab to make something that we can use to make learning more fun and interative.

[Home](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/home.md?ref_type=heads)   | [About our project](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/dna.md?ref_type=heads) | [Team](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/Team/Team.md?ref_type=heads) | [Terminology](https://gitlab.com/project-work222511/dna/dna/-/blob/main/MAIN/HOME/PROJECT/Team/terminology/Trem.md?ref_type=heads)







