
![](main.jpeg)

# DNA: The heir of inheritance

### 1. Firstly we made our DNA model with specification of every segment of the DNA(Adenine, Guanine, cytosine and thymine). In the TinkerCAD,(https://www.tinkercad.com/things/dViCTVm0tge-fantabulous-duup-kup/edit?sharecode=ljN-68rblYu1eEHQ6MoExQhlDbS_1tte-joyfQjUtbU)

![](tinker.png)

![](side%20.png)

### 2. Then we designed our DC motor circuit with pushbutton to control it, in the TinkerCAD. 

![](circuit.png)




